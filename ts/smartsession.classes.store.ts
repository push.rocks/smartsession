import * as plugins from './smartsession.plugins';

export interface storageObject {
  [key: string]: any
}

/**
 * SessionStore is in charge of storing session related data
 */
export class SessionStore<T> {
  memoryStore: storageObject = {};
  
  /**
   * check for an id
   */
  checkForId(idArg: string): boolean {
    if(this.memoryStore[idArg]) {
      return true;
    }
    return false
  }
  
  /**
   * gets an id
   */
  getId(idArg: string): T {
    return this.memoryStore[idArg];
  }
  
  /**
   * 
   */
  removeId(idArg: string): boolean {
    // TODO: implement
    if(this.checkForId(idArg)) {
      delete this.memoryStore[idArg];
      return true
    }
    return false
  }
  
  /**
   * updates an id within store
   */
  updateId(idArg: string, payloadArg: T) {
    if(this.checkForId(idArg)) {
      this.memoryStore[idArg] = payloadArg;
    }
  }
  
  /**
   * upserts an id within store
   */
  upsertId(idArg: string, payloadArg: T) {
    this.memoryStore[idArg] = payloadArg;
  }

  /**
   * syncs the fast memory store with a persistence layer in an async way
   * TODO: Think about what actions need to be blocking to ensure cluster availability
   */
  sync() {}
};
